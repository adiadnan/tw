class Query {
    artwork_name: string; /* required */
    author: string; /* required */
    year: string;
    type: string;
    country: string;
}

class QueryResponse {
    results: Array<{
        artwork_name: string;
        author: string;
        year: string;
        type: string;
        country: string;
        unmatched_data: [{
            'key': 'value'
        }]
    }>;
}

class Stats {
    daily_similar_searches: number;
    weekly_similar_searches: number;
    monthly_similar_searches: number;
    author_similar_searches: number;
    author_summary: string;
    type_similar_searches: string;
    author_publication_count: number;
    last_purchase_price: number;
}