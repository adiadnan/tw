{
  "swagger" : "2.0",
  "info" : {
    "description" : "The main endpoints for the Ark project.",
    "version" : "1.0.0",
    "title" : "Wade Ark API",
    "contact" : {
      "email" : "andrei.constantin016@gmail.com"
    },
    "license" : {
      "name" : "Apache 2.0",
      "url" : "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host" : "virtserver.swaggerhub.com",
  "basePath" : "/wade_tw/Wade_tw/1.0.0",
  "schemes" : [ "https", "http" ],
  "paths" : {
    "/db" : {
      "get" : {
        "summary" : "Query the DBPedia endpoint.",
        "description" : "Searches the DBPedia or any other RDF-based online database and returns the results in the form of artworks.(i.e. movie, song, painting, sculpture, etc.)\n",
        "operationId" : "queryDBPedia",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "artwork_name",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "author",
          "in" : "query",
          "description" : "the author of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "year",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "type",
          "in" : "query",
          "description" : "the type of the artwork(i.e. movie, painting, sculpture)",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "search results matching criteria",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/Artwork"
              }
            }
          },
          "400" : {
            "description" : "bad input parameter"
          },
          "404" : {
            "description" : "resource not found"
          }
        }
      }
    },
    "/artwork" : {
      "get" : {
        "summary" : "Find available artwork.",
        "description" : "Searches for the available data provided by the /db endpoint to get the processed results.\n",
        "operationId" : "queryDatabase",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "artwork_name",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "author",
          "in" : "query",
          "description" : "the author of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "year",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "type",
          "in" : "query",
          "description" : "the type of the artwork(i.e. movie, painting, sculpture)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "country",
          "in" : "query",
          "description" : "the countries that are often associated with the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "location",
          "in" : "query",
          "description" : "the current location of the artwork(i.e. Paris,Vienna)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "location created",
          "in" : "query",
          "description" : "the location where the artwork was created(i.e. Madrid)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "about",
          "in" : "query",
          "description" : "describes what the artwork is about",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "genre",
          "in" : "query",
          "description" : "the genre of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "publisher",
          "in" : "query",
          "description" : "the publisher of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "award",
          "in" : "query",
          "description" : "the award won by the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "based on",
          "in" : "query",
          "description" : "describes the artwork foundation",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "search results matching criteria",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/Artwork"
              }
            }
          },
          "400" : {
            "description" : "bad input parameter"
          },
          "404" : {
            "description" : "resource not found"
          }
        }
      }
    },
    "/artwork/detailed" : {
      "get" : {
        "summary" : "Find additional details about a particular artwork.",
        "description" : "Searches for the available data provided by the /db endpoint to get the processed results.\n",
        "operationId" : "detailedQueryDatabase",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "artwork_name",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "author",
          "in" : "query",
          "description" : "the author of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "year",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "type",
          "in" : "query",
          "description" : "the type of the artwork(i.e. movie, painting, sculpture)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "location",
          "in" : "query",
          "description" : "the current location of the artwork(i.e. Paris,Vienna)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "location created",
          "in" : "query",
          "description" : "the location where the artwork was created(i.e. Madrid)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "about",
          "in" : "query",
          "description" : "describes what the artwork is about",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "genre",
          "in" : "query",
          "description" : "the genre of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "publisher",
          "in" : "query",
          "description" : "the publisher of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "award",
          "in" : "query",
          "description" : "the award won by the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "based on",
          "in" : "query",
          "description" : "describes the artwork foundation",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "search results matching criteria",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/ArtworkDetailed"
              }
            }
          },
          "400" : {
            "description" : "bad input parameter"
          },
          "404" : {
            "description" : "resource not found"
          }
        }
      }
    },
    "/stats" : {
      "get" : {
        "summary" : "Get overall statistics.",
        "description" : "Get overall statistics. For example similar queries made by other users today, this week, this month, etc.\n",
        "operationId" : "getStats",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "artwork_name",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "author",
          "in" : "query",
          "description" : "the author of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "year",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "type",
          "in" : "query",
          "description" : "the type of the artwork(i.e. movie, painting, sculpture)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "location",
          "in" : "query",
          "description" : "the current location of the artwork(i.e. Paris,Vienna)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "location created",
          "in" : "query",
          "description" : "the location where the artwork was created(i.e. Madrid)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "about",
          "in" : "query",
          "description" : "describes what the artwork is about",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "genre",
          "in" : "query",
          "description" : "the genre of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "publisher",
          "in" : "query",
          "description" : "the publisher of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "award",
          "in" : "query",
          "description" : "the award won by the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "based on",
          "in" : "query",
          "description" : "describes the artwork foundation",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "statistics matching the query",
            "schema" : {
              "$ref" : "#/definitions/Stats"
            }
          },
          "400" : {
            "description" : "bad input parameter"
          },
          "404" : {
            "description" : "resource not found"
          }
        }
      }
    },
    "/recommend" : {
      "get" : {
        "summary" : "Get recommendations.",
        "description" : "Get recommendations based on user query. For instance recommendations include artworks made by same artist.\n",
        "operationId" : "getRecommendations",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "artwork_name",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "author",
          "in" : "query",
          "description" : "the author of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "year",
          "in" : "query",
          "description" : "the name of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "type",
          "in" : "query",
          "description" : "the type of the artwork(i.e. movie, painting, sculpture)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "location",
          "in" : "query",
          "description" : "the current location of the artwork(i.e. Paris,Vienna)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "location created",
          "in" : "query",
          "description" : "the location where the artwork was created(i.e. Madrid)",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "about",
          "in" : "query",
          "description" : "describes what the artwork is about",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "genere",
          "in" : "query",
          "description" : "the genre of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "publisher",
          "in" : "query",
          "description" : "the publisher of the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "award",
          "in" : "query",
          "description" : "the award won by the artwork",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "based on",
          "in" : "query",
          "description" : "describes the artwork foundation",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "recommendations matching the query",
            "schema" : {
              "$ref" : "#/definitions/Recommend"
            }
          },
          "400" : {
            "description" : "bad input parameter"
          },
          "404" : {
            "description" : "resource not found"
          }
        }
      }
    }
  },
  "definitions" : {
    "Artwork" : {
      "type" : "object",
      "required" : [ "artwork_name", "author" ],
      "properties" : {
        "artwork_name" : {
          "type" : "string",
          "example" : "Mona Lisa, The starry night"
        },
        "author" : {
          "type" : "string",
          "example" : "Leonardo Da Vinci, Vincent Van Gogh"
        },
        "year" : {
          "type" : "string",
          "format" : "date | period | year",
          "example" : "1600-1650, 1720"
        },
        "type" : {
          "type" : "string",
          "example" : "movie, painting, etc."
        },
        "location" : {
          "type" : "string",
          "example" : "Paris,London etc."
        },
        "locationCreated" : {
          "type" : "string",
          "example" : "Prague,Paris,Amsterdam etc."
        }
      }
    },
    "ArtworkDetailed" : {
      "type" : "object",
      "required" : [ "artwork_name", "author", "id" ],
      "properties" : {
        "id" : {
          "type" : "string",
          "format" : "uuid"
        },
        "artwork_name" : {
          "type" : "string",
          "example" : "Mona Lisa, The starry night"
        },
        "abstract" : {
          "type" : "string",
          "example" : "1066 and All That A Memorable History of England,..."
        },
        "language" : {
          "type" : "string",
          "example" : "English"
        },
        "comment" : {
          "type" : "string",
          "example" : "2600 The Hacker Quarterly is an American seasonal publication"
        },
        "literaryGenre" : {
          "type" : "string",
          "example" : "Parody"
        },
        "number_of_pages" : {
          "type" : "integer",
          "format" : "int32"
        },
        "publisher" : {
          "type" : "string",
          "example" : "Methuen_Publishing"
        },
        "author" : {
          "type" : "string",
          "example" : "Leonardo Da Vinci, Vincent Van Gogh"
        },
        "owner" : {
          "type" : "string",
          "example" : "John Doe, The Louvre Museum"
        },
        "year" : {
          "type" : "string",
          "format" : "date | period | year",
          "example" : "1600-1650, 1720"
        },
        "type" : {
          "type" : "string",
          "example" : "movie, painting, etc."
        },
        "note" : {
          "type" : "string",
          "example" : "Leonardo was born near Vinci, at Anchiano. He is known as the founder of the High Renaissance style."
        }
      }
    },
    "Stats" : {
      "type" : "object",
      "required" : [ "weekly_similar_searches" ],
      "properties" : {
        "daily_similar_searches" : {
          "type" : "number",
          "format" : "int32"
        },
        "weekly_similar_searches" : {
          "type" : "number",
          "format" : "int32"
        },
        "monthly_similar_searches" : {
          "type" : "number",
          "format" : "int32"
        },
        "author_similar_searches" : {
          "type" : "number",
          "format" : "int32"
        },
        "year_similar_searches" : {
          "type" : "number",
          "format" : "int32"
        },
        "type_similar_searches" : {
          "type" : "number",
          "format" : "int32"
        },
        "author_publication_count" : {
          "type" : "number",
          "format" : "int32",
          "description" : "How many publications this author has."
        },
        "author_summary" : {
          "type" : "string",
          "description" : "Details about the author."
        },
        "last_purchase_price" : {
          "type" : "number",
          "format" : "int64",
          "description" : "The last auction price of the painting."
        }
      }
    },
    "Recommend" : {
      "type" : "object",
      "required" : [ "artworks_by_same_artist" ],
      "properties" : {
        "artworks_by_same_artist" : {
          "$ref" : "#/definitions/Artwork"
        },
        "artworks_same_period" : {
          "$ref" : "#/definitions/Artwork"
        },
        "artworks_by_collaborator" : {
          "$ref" : "#/definitions/Artwork"
        },
        "artworks_in_same_area" : {
          "$ref" : "#/definitions/Artwork"
        }
      }
    }
  }
}