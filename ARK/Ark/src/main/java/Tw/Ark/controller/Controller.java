package Tw.Ark.controller;

import Tw.Ark.entities.Request;
import Tw.Ark.entities.Response;
import Tw.Ark.service.DbpediaService;
import org.apache.jena.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class Controller {

    @Autowired
    private DbpediaService dbpediaService;
    
    @CrossOrigin
    @RequestMapping("/search")
    @PostMapping
    public @ResponseBody List<Response> response(@RequestBody Request req) {
        StringBuilder query = dbpediaService.generateQuery(req);
        List<Response> result=dbpediaService.executeQuery(query.toString());
        return result;
    }
    
    @CrossOrigin
    @RequestMapping("/statistics")
    @PostMapping
    public @ResponseBody Map<String,Integer> statistics(@RequestBody Request req) {
    	Map<String,Integer> statistics=new HashMap();
    	Integer sameAuthor=0;
    	Integer samePeriod=0;
    	Integer sameTypeAndPeriod=0;
    	Integer sameLocation=0;
    	if(req.getAuthor()!=null&&req.getAuthor()!="")
    	{sameAuthor=response(new Request(null,req.getAuthor(),null,null,null,null,null)).size();
    	statistics.put("sameAuthor", sameAuthor);}
    	if(req.getYearRange()!=null&&req.getYearRange()!="")
    	{samePeriod=response(new Request(null,null,req.getYearRange(),null,null,null,null)).size();
    	statistics.put("samePeriod", samePeriod);}
    	if(req.getYearRange()!=null&&req.getYearRange()!=""&&req.getType()!=null&&req.getType()!=null&&req.getType()!="")
    	{sameTypeAndPeriod=response(new Request(null,null,req.getYearRange(),null,null,null,req.getType())).size();
    	statistics.put("sameTypeAndPeriod", sameTypeAndPeriod);}
    	if(req.getLocation()!=null&&req.getLocation()!="")
    	{sameLocation=response(new Request(null,null,null,req.getLocation(),null,null,null)).size();
    	statistics.put("sameLocation", sameLocation);}
    	return statistics;
    }
    
}