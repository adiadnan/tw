package Tw.Ark.entities;

public class Response {

	private String name;
	private String author;
	private String year;
	private String location;
	private String description;
	private String publisher;
	private String type;
	private Object image;
	private Integer wikiId;
	
	
	public Integer getWikiId() {
		return wikiId;
	}

	public void setWikiId(Integer wikiId) {
		this.wikiId = wikiId;
	}

	public Object getImage() {
		return image;
	}

	public void setImage(Object object) {
		this.image = object;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String yearRange) {
		this.year = yearRange;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public void setType(String type) {
		this.type=type;
		
	}
	public String getType() {
		return type;
	}
}
