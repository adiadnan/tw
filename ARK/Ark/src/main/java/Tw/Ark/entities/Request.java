package Tw.Ark.entities;

public class Request {

	private String name;
	private String author;
	private String yearRange;
	private String location;
	private String description;
	//private String genre;
	private String publisher;
	private String type;
//	private String award;
//	private String basedOn;
//	private String locationCreated;

	
	public String getName() {
		return name;
	}

	public Request(String name, String author, String yearRange, String location, String description, String publisher,
			String type) {
		super();
		this.name = name;
		this.author = author;
		this.yearRange = yearRange;
		this.location = location;
		this.description = description;
		this.publisher = publisher;
		this.type = type;
	}
	
	public Request() {};

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getYearRange() {
		return yearRange;
	}

	public void setYearRange(String yearRange) {
		this.yearRange = yearRange;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

}