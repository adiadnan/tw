package Tw.Ark.service;

import Tw.Ark.entities.Request;
import Tw.Ark.entities.Response;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.ResourceFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DbpediaService {

	private Request req;

	private Map<String, Integer> types = new HashMap();
	private List<String> listOfTypes = Arrays.asList("portrait","sculpture", "sculptural", "picture", "statue", "painting", "statue", "church",
			"monument");

	public DbpediaService() {
		resetTypes();
	}

	private void resetTypes() {
		for (int i = 0; i < listOfTypes.size(); i++)
			types.put(listOfTypes.get(i), 0);

	}
	

	public StringBuilder generateQuery(Request request) {
		req = request;
		StringBuilder sb = new StringBuilder("PREFIX dbo: <http://dbpedia.org/ontology/>\n"
				+ "PREFIX dbp: <http://dbpedia.org/property/>\n" + "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
				+ "select distinct ?name ?authorName ?year ?cityName ?about ?museumName ?thumbnail ?wikiId where {\n"
				+ "?artWork a dbo:Artwork.\n" + "OPTIONAL {?artWork foaf:name ?name}\n"
				+ "OPTIONAL {?artWork dbo:thumbnail ?thumbnail}\n" + "?artWork foaf:name ?artName.\n"
				+ "OPTIONAL {?artWork dbo:author ?author}\n" + "?author foaf:name ?authorName.\n"
				+ "OPTIONAL {?artWork dbp:year ?year}\n" + "OPTIONAL {?artWork dbp:city ?city}\n"
				+ "   ?city foaf:name ?cityName.\n" + "OPTIONAL {?artWork dbo:abstract ?about}\n"
				+ "OPTIONAL {?artWork dbo:museum ?museum}\n" + "?museum foaf:name ?museumName.\n" 
				+ "{?artWork dbo:wikiPageID ?wikiId} \n"+
				

				"FILTER (langMatches(lang(?museumName), \"EN\"))\n" + "FILTER (langMatches(lang(?about), \"EN\"))\n"
				+ "FILTER (langMatches(lang(?cityName), \"EN\"))\n"
				+ "FILTER (langMatches(lang(?authorName), \"EN\"))\n"
				+ "FILTER (langMatches(lang(?artName), \"EN\"))\n");

		if (req.getName() != null && req.getName() != "") {
			sb.append("FILTER contains(lcase(?artName),\"" + req.getName().toLowerCase() + "\").\n");
		}
		if (req.getAuthor() != null && req.getAuthor() != "") {
			sb.append("   FILTER contains(lcase(?authorName),\"" + req.getAuthor().toLowerCase() + "\").");
		}
		if (req.getYearRange() != null) {
			String[] years = req.getYearRange().split("-");
			if (years.length == 2) {
				sb.append("FILTER(?year >= " + years[0] + " && ?year <= " + years[1] + ").\n");
			}
		}

		if (req.getLocation() != null && req.getLocation() != "") {
			sb.append("   FILTER contains(lcase(?cityName),\"" + req.getLocation().toLowerCase() + "\").\n");
		}
		if (req.getDescription() != null && req.getDescription() != "") {
			sb.append("FILTER contains(lcase(?about),\"" + req.getDescription().toLowerCase() + "\").\n");
		}

		if (req.getType() != null && req.getType() != "") {
			sb.append("FILTER contains(lcase(?about),\"" + req.getType().toLowerCase() + "\").\n");
		}

		if (req.getPublisher() != null && req.getPublisher() != "") {
			sb.append("FILTER contains(lcase(?museumName),\"" + req.getPublisher().toLowerCase() + "\").\n");
		}

		// end of query
		sb.append("} LIMIT 100");

		return sb;
	}

	public List<Response> executeQuery(String query) {
		List<Response> responseList = new ArrayList<>();
		ParameterizedSparqlString qs = new ParameterizedSparqlString(query);

		QueryExecution exec = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", query);
		ResultSet results = ResultSetFactory.copyResults(exec.execSelect());
		while (results.hasNext()) {
			Response response = new Response();
			QuerySolution querySolution = results.next();
			if (validateType(querySolution.getLiteral("about"), req.getType())) {
				if (querySolution.get("name") instanceof Literal)
					response.setName(querySolution.getLiteral("name") == null ? ""
							: querySolution.getLiteral("name").getString());
				if (querySolution.get("authorName") instanceof Literal)
					response.setAuthor(querySolution.getLiteral("authorName") == null ? ""
							: querySolution.getLiteral("authorName").getString());
				if (querySolution.get("year") instanceof Literal)
					response.setYear(querySolution.getLiteral("year") == null ? ""
							: querySolution.getLiteral("year").getString());
				if (querySolution.get("cityName") instanceof Literal)
					response.setLocation(querySolution.getLiteral("cityName") == null ? ""
							: querySolution.getLiteral("cityName").getString());
				if (querySolution.get("about") instanceof Literal)
					response.setDescription(querySolution.getLiteral("about") == null ? ""
							: querySolution.getLiteral("about").getString());
				if (req.getType() != null && req.getType() != "")
					response.setType(req.getType());
				else
					response.setType(findType(response.getDescription()));

				if (querySolution.get("museumName") instanceof Literal)
					response.setPublisher(querySolution.getLiteral("museumName") == null ? ""
							: querySolution.getLiteral("museumName").getString());
				response.setImage(querySolution.getResource("thumbnail") == null ? ""
						: querySolution.getResource("thumbnail").getURI());
				response.setWikiId(querySolution.get("wikiId").asLiteral().getInt());
				responseList.add(response);
			}
		}
		return responseList;
	}


	private boolean validateType(Literal description, String type) {
		if (type != null && type != "")
			return findType(description.getString()).equals(type);
		return true;
	}

	private String findType(String description) {
		String value = "";
		if (description == null)
			return "";
		else {
			if (description.replaceFirst("\\.", "").contains("."))
				description = description.replaceFirst("\\.", "").substring(0, description.indexOf("."));
			int occurences;
			for (String type : listOfTypes) {
				occurences = 0;
				while (description.contains(type)) {
					occurences = types.get(type);
					occurences++;
					types.put(type, occurences);
					description = description.replace(type, "");
				}
			}

			int max = 0;
			for (int i = 0; i < listOfTypes.size(); i++)
				if (types.get(listOfTypes.get(i)) > max) {
					max = types.get(listOfTypes.get(i));
					value = listOfTypes.get(i);
				}

		}
		resetTypes();
		return value;
	}

}
