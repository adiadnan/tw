package Tw.Ark;
import Tw.Ark.entities.Request;
import Tw.Ark.entities.Response;
import Tw.Ark.service.DbpediaService;
import org.apache.jena.query.Query;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

	@SpringBootApplication
	public class App {

	    public static void main(String[] args) {
	        SpringApplication.run(App.class, args);

//			DbpediaService dbpediaService = new DbpediaService();
//			Request request = new Request();
//			request.setName("Mona Lisa");
//			request.setAuthor("Leonardo da Vinci");
//			request.setYearRange("1500-2000");
//			request.setType("person");
//			request.setLocation("Paris");
//			request.setDescription("Francesco del Giocondo");
//			request.setPublisher("Louvre");
//			StringBuilder sb = dbpediaService.generateQuery(request);
//			Response response = dbpediaService.executeQuery(sb.toString());
	    }
	}

