import { Injectable } from '@angular/core';

import { ArtworkModel } from '../models/ArtworkModel';
import { StatsDto } from '../models/dto/StatsDto';
import { ArtworkDto } from '../models/dto/ArtworkDto';

@Injectable()
export class CommonDataService {
	private _results: ArtworkDto[];
	private _recommendations: ArtworkDto[];
	private _statistics: StatsDto[];
	private _artwork: ArtworkModel;

	public get recommendations(): ArtworkDto[] {
		return this._recommendations;
	}

	public set recommendations(value: ArtworkDto[]) {
		this._recommendations = value;
	}

	public get results(): ArtworkDto[] {
		return this._results;
	}

	public set results(value: ArtworkDto[]) {
		this._results = value;
	}

	public get artwork(): ArtworkModel {
		return this._artwork;
	}

	public set artwork(value: ArtworkModel) {
		this._artwork = value;
	}


	public get statistics(): StatsDto[] {
		return this._statistics;
	}

	public set statistics(value: StatsDto[]) {
		this._statistics = value;
	}


}