import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';

import 'rxjs/add/operator/map';

import { Observable } from 'rxjs';

import { ArtworkModel } from '../models/ArtworkModel';
import { ArtworkDto } from '../models/dto/ArtworkDto';
import { StatsDto } from '../models/dto/StatsDto';
import { CommonDataService } from './common-data.service';

@Injectable()
export class SearchService {

  public RECOMMENDATIONS_CONFIG: string[] = ['author', 'type', 'location'];

  /*
  autor + tip + locatie completate 
autor + tip 
autor + locatie 
tip+ locatie
autor
tip */

  constructor(
    private Http: Http,
    private CommonDataService: CommonDataService
  ) { }

  public getArtworks(artwork: ArtworkModel): Observable<ArtworkDto[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.Http.post(`${environment.endpoint}/search`, artwork, options).map(res => res.json());
  }

  public getArtworksRecommendations(artwork: ArtworkModel): void {
    this.CommonDataService.recommendations = [];
    let firstStepFail: boolean =  true;
    let artworkKeys: string[] = Object.keys(artwork);
    for (let i = 0; i < artworkKeys.length; i++) {
      if (artwork[artworkKeys[i]].length > 0 && this.RECOMMENDATIONS_CONFIG.indexOf(artworkKeys[i]) > -1) {
        firstStepFail = false;
        let _artwork: ArtworkModel = new ArtworkModel();
        _artwork[artworkKeys[i]] = artwork[artworkKeys[i]];
        this.getArtworks(_artwork).subscribe(success => {
          if (success != null && success.length > 0) {
            this.CommonDataService.recommendations.concat(success);
          }
        });
      }
    }

    // if(firstStepFail) {
    //   let status: Array<{ key: string, value: string, count: number }> = new Array<{ key: string, value: string, count: number }>();
    //   for(let i = 0; i < this.CommonDataService.results.length; i++) {
    //     let keys = Object.keys(this.CommonDataService.results);

    //   }
    //   status.forEach((value, key) => {
    //     let _artwork: ArtworkModel = new ArtworkModel();
    //     _artwork[key] = key;
    //     this.getArtworks(_artwork).subscribe(success => {
    //       if (success != null && success.length > 0) {
    //         this.CommonDataService.recommendations.concat(success);
    //       }
    //     });
    //   })
    // }
  }


  public getStats(artwork: ArtworkModel): Observable<StatsDto> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.Http.post(`${environment.endpoint}/statistics`, artwork, options).map(res => res.json());
  }

  public getArtworksStatistics(): Observable<ArtworkDto[]> {
    return this.Http.get(`${environment.endpoint}`).map(res => res.json());
  }
}
