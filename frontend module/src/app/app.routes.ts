// ====== ./app/app.routes.ts ======

// Imports
// Deprecated import
// import { provideRouter, RouterConfig } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ActivatedRoute, Routes, RouterModule } from '@angular/router';
import { InitialFormComponent } from './components/initial-form/initial-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';

// Route Configuration
export const routes: Routes = [
    {
        path: '',
        component: InitialFormComponent
    },
    {
        path: 'search',
        component: SearchResultsComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);