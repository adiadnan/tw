import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';  // <-- #1 import module
import { HttpModule } from '@angular/http';
/* Third-party software */
import { AgmCoreModule } from '@agm/core';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';

import { GMapsService } from '../services/gmaps.service';
import { Minify } from '../pipes/minify.pipes';

/* #1 Import custom modules */

/* #2 Import custom components */
import { WadeMapsComponent } from '../components/wade-maps/wade-maps.component';
import { InitialFormComponent } from '../components/initial-form/initial-form.component';
import { SearchResultsComponent } from '../components/search-results/search-results.component';

/* #3 Import custom services */
import { SearchService } from '../services/search-service.service';
import { CommonDataService } from '../services/common-data.service';

@NgModule({
  declarations: [
    InitialFormComponent,
    SearchResultsComponent,
    WadeMapsComponent,
    Minify
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCbMS6nYxv0njrCdyu--NUifleXt2OiVyU'
    }),
    AgmSnazzyInfoWindowModule
  ],
  exports: [
    WadeMapsComponent,
    InitialFormComponent
  ],
  providers: [
    SearchService,
    CommonDataService,
    GMapsService
  ]
})
export class GeneralModule { }
