import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { SearchService } from '../../services/search-service.service';
import { CommonDataService } from '../../services/common-data.service';
import { ArtworkModel } from '../../models/ArtworkModel';

@Component({
  selector: 'initial-form',
  templateUrl: './initial-form.component.html',
  styleUrls: ['./initial-form.component.css']
})
export class InitialFormComponent {
  private searchForm: FormGroup;
  private MIN_INPUT_LENGTH = 2;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private SearchService: SearchService,
    private CommonDataService: CommonDataService
  ) {
    this.createForm();
  }

  private createForm(): void {
    this.searchForm = this.formBuilder.group({
      name: '',
      author: '',
      year: '',
      type: '',
      location: '',
      description: '',
      genre: '',
      publisher: ''
    });
  }

  private onSubmit(): void {
    let searchObject: ArtworkModel = new ArtworkModel();
    searchObject.name = this.searchForm.get('name').value;
    searchObject.author = this.searchForm.get('author').value;
    searchObject.description = this.searchForm.get('description').value;
    searchObject.location = this.searchForm.get('location').value;
    searchObject.publisher = this.searchForm.get('publisher').value;
    searchObject.type = this.searchForm.get('type').value;
    searchObject.year = this.searchForm.get('year').value;
    this.CommonDataService.artwork = searchObject;
    this.SearchService.getArtworks(searchObject).subscribe(
      success => {
        this.CommonDataService.results = success;
        this.router.navigateByUrl('/search');
      });
  }

  private formIsValid(): boolean {
    let controls: string[] = Object.keys(this.searchForm.controls);
    let atLeastOneFormValid: boolean = false;
    for (let index = 0; index < controls.length; index++) {
      if(this.searchForm.get(controls[index]).value.length > this.MIN_INPUT_LENGTH) {
        atLeastOneFormValid = true;
      }
    }
    return atLeastOneFormValid;
  }

}
