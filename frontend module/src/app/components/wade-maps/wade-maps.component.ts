import { Component, OnInit, OnChanges, SimpleChanges, Input, ViewChild } from '@angular/core';
import { AgmMap, AgmMarker, LatLng } from '@agm/core';
import { GMapsService } from '../../services/gmaps.service';
import { NgZone } from '@angular/core';
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';

@Component({
  selector: 'wade-maps',
  templateUrl: './wade-maps.component.html',
  styleUrls: ['./wade-maps.component.css']
})
export class WadeMapsComponent implements OnInit, OnChanges {
  private latitude: number = 51.678418;
  private longitude: number = 7.809007;
  @Input() markers: Array<{ address: string, label: string }>;
  private actualMarkers: Array<{ label: string, latitude: number, longitude: number }>;
  @ViewChild('map') map: AgmMap;

  constructor(private GMapsService: GMapsService, private _zone: NgZone) {
    this.actualMarkers = new Array<{ latitude: number, longitude: number, label: string }>();
  }

  ngOnInit() {
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if ('markers' in simpleChanges && simpleChanges['markers'].currentValue != null) {
      this.refreshPage();
    }
  }

  private refreshPage(): void {
    this.actualMarkers = new Array<{ latitude: number, longitude: number, label: string }>();
    
    this.markers.forEach(item => {
      this.GMapsService.getGeocoding(item.address).subscribe(
        success => {
          // this._zone.run(() => {
          let latitude = success.lat();
          let longitude = success.lng();
          this.actualMarkers.push({ latitude: latitude, longitude: longitude, label: item.label });
          // });
        }
      );
    });
  }
}
