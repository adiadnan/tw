import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { SearchService } from '../../services/search-service.service';

import { ArtworkModel } from '../../models/ArtworkModel';
import { ArtworkDto } from '../../models/dto/ArtworkDto';
import { CommonDataService } from '../../services/common-data.service';
import { WadeMapsComponent } from '../wade-maps/wade-maps.component';
import { StatsDto } from '../../models/dto/StatsDto';

enum Menu {
  RESULTS,
  RECOMMENDATIONS,
  STATISTICS,
  LOADING
}

@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {
  private results: ArtworkDto[];
  private recommendations: ArtworkDto[];
  private stats: StatsDto = new StatsDto();
  MIN_CHAR: number = 500;

  private markers: Array<{ address: string, label: string }>;

  private address: string;

  private userSelectionEnum = Menu;
  private userSelection: number = Menu.RESULTS;

  @ViewChild('chart') private chart;
  @ViewChild('map') private map: WadeMapsComponent;

  constructor(
    private SearchService: SearchService,
    private CommonDataService: CommonDataService,
    private router: Router
  ) {
    if (this.CommonDataService.artwork == null) {
      this.router.navigateByUrl('/');
    }
  }

  isGood(): boolean {
    return Object.keys(this.stats).length === 0 && this.stats.constructor === Object
  }

  ngOnInit() {
    this.results = this.CommonDataService.results;
    this.recommendations = this.CommonDataService.recommendations;
    // this.SearchService.getArtworksRecommendations().subscribe(succes => this.recommendations = succes);
  }

  toggleDescription(artwork: ArtworkDto) {
    artwork.selected = !artwork.selected;
  }

  private changeTab(userSelection: number): void {
    this.recommendations = new Array<ArtworkDto>();
    this.userSelection = userSelection;
    let self = this;
    if (userSelection == this.userSelectionEnum.RECOMMENDATIONS) {
      let artworkKeys: string[] = Object.keys(this.CommonDataService.artwork);
      let availableStuff: number = artworkKeys.filter(item => {
        return this.CommonDataService.artwork[item].length > 0;
      }).length;
      if(availableStuff == 1) {
        let nothingWorked: boolean = true;
        for (let i = 0; i < artworkKeys.length; i++) {
          if (this.CommonDataService.artwork[artworkKeys[i]].length > 0 && this.SearchService.RECOMMENDATIONS_CONFIG.indexOf(artworkKeys[i]) > -1) {
            nothingWorked = false;
            let _artwork: ArtworkModel = new ArtworkModel();
            _artwork[artworkKeys[i]] = this.CommonDataService.artwork[artworkKeys[i]];
            this.SearchService.getArtworks(_artwork).subscribe(success => {
              if (success != null && success.length > 0) {
                self.recommendations = self.recommendations.concat(success);
              }
            });
          }
        }
      }
    } else if (userSelection == this.userSelectionEnum.STATISTICS) {
      this.SearchService.getStats(this.CommonDataService.artwork)
        .subscribe(success => {
          this.stats = success;
          this.markers = this.CommonDataService.results.map(item => {
            return {
              address: item.location,
              label: item.publisher
            }
          });
        });
    }
  }
}
