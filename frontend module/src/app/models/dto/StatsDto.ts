export class StatsDto {
    sameAuthor: number;
    sameLocation: number;
    samePeriod: number;
    sameTypeAndPeriod: number;
}