export class ArtworkDto {
    public wikiId?: string;
    public name?: string; 
    public author?: string; /* for recommendations */
    public year?: string; 
    public description?: string;
    public publisher?: string;
    public type?: string; /* for recommendations */
    public image?: string;
    public location?: string; /* for recommendations */
    public selected: boolean = false;
}