export class ArtworkModel {
    public name?: string;
    public author?: string;
    public year?: string;
    public description?: string;
    public publisher?: string;
    public type?: string;
    public location?: string;
}